<?php
/**
 * The class that defines the settings / default values for the plugin.
 *
 * @package WPezGutenBetterImage\App\Config
 */

namespace WPezGutenBetterQuoteShare\App\Config;

/**
 * {@inheritDoc}
 */
class ClassConfig implements InterfaceConfig {

	/**
	 * The plugin's dir.
	 *
	 * @var string
	 */
	private $plugin_dir;

	/**
	 * The plugin's url.
	 *
	 * @var string
	 */
	private $plugin_url;

	/**
	 * The plugin's slug, implode()'ed with a separator.
	 *
	 * @var array
	 */
	private $plugin_slug;


	/**
	 * Array that stores the slugs, indexed by the separator.
	 *
	 * @var array
	 */
	private $arr_plugin_slugs;

	private $arr_js_general;

	private $arr_js_networks;

	private $arr_selectors;

	private $str_cite_alt_author_meta_key;

	private $str_current_user_can_capability;

	private $bool_enqueue_scripts;

	private $bool_add_inline_script;

	private $bool_enqueue_styles;

	private $bool_qs_elements;


	/**
	 * Construct the config.
	 *
	 * @param string $plugin_dir The plugin's dir.
	 * @param string $plugin_url The plugin's url.
	 */
	public function __construct( string $plugin_dir, string $plugin_url ) {

		$this->plugin_dir = $plugin_dir;

		$this->plugin_url = $plugin_url;

		$this->setPropertyDefaults();

	}

	/**
	 * Set the various property defaults.
	 *
	 * @return void
	 */
	private function setPropertyDefaults() {

		$this->arr_js_general = array(
			'ns'                => new \stdClass(),
			'active'            => true,
			'fileNameSlug'      => 'file-name-slug',
			'blockquoteCiteAlt' => 'used for the alt Cite button',
			'source_prefix'     => 'Source: ',
			'source'            => 'used for the Source',
			'gStyles'           => array(
				'active' => true,
				'slug'   => 'style-',
				'start'  => 1,
				'end'    => 24,
			),
			'h2cScale'           => 1,  // your value or 'default' to use hTML2Canvas' default.
		);

		$this->arr_js_networks = array(
			'feed_fb'  => array(
				'maxH' => 500,
			),
			'feed_ig'  => array(
				'maxH' => 800,
			),
			'feed_li'  => array(
				'maxH' => 500,
			),
			'feed_pin' => array(
				'maxH' => 1000,
			),
			'feed_tw'  => array(
				'maxH' => 600,
			),
			'story'    => array(
				'maxH' => 1500,
			),
		);

		$this->arr_js_selectors = array(

			'body'              => 'body',
			'qsEles'            => '.wpez-qs-elements',
			'overlay'           => '.wpez-qs-overlay',
			'controls'          => '.wpez-qs-controls',
			'classOverlayOpen'  => 'wpez-qs-overlay-open-true',  // class - no leading . necessary.
			'toCanvas'          => '.wpez-qs-to-canvas',
			'share'             => '.wpez-qs-share-wrap',
			'shareShow'         => '.wpez-qs-show-networks',
			'shareNetworks'     => '.wpez-qs-networks-wrap',
			'quotes'            => '.wp-block-pullquote, .wp-block-quote, #wpez-h2c',
			'quotesEx'          => 'wpez-qs-false',  // ex = exclude
			'btnShow'           => 'button.wpez-qs-networks-show',
			'classbBtnShowHide' => 'wpez-qs-networks-display-false',  // class - no leading . necessary.
			'networkWrap'       => '.wpez-qs-networks-wrap',
			'networkEle'        => 'button',
			'classCiteAlt'      => 'wpez-qs-cite-alt-true',
			'classCiteRemove'   => 'wpez-qs-cite-display-none-true',
			'source'            => '.wpez-qs-source',
		);

		$this->str_cite_alt_author_meta_key = 'display_name';
		$this->str_current_user_can_capability = 'edit_posts';

		$this->bool_enqueue_scripts   = true;
		$this->bool_add_inline_script = true;
		$this->bool_enqueue_styles    = true;
		$this->bool_qs_elements       = true;
	}


	/**
	 * {@inheritDoc}
	 */
	public function getPluginDir() : string {

		return trim( $this->plugin_dir );
	}

	// new
	public function getPluginURL() : string {

		return trim( $this->plugin_url );
	}


	public function getJSGeneral() : array {

		return $this->arr_js_general;
	}

	
	public function getJSNetworks() : array {

		return $this->arr_js_networks;
	}


	public function getJSSelectors() : array {

		return $this->arr_js_selectors;
	}

	public function getCiteAltAuthorMetaKey() : string {

		return $this->str_cite_alt_author_meta_key;
	}

	public function getCurrentUserCanCapability() : string {
		
		return $this->str_current_user_can_capability;
	}

	public function getEnqueueScripts() : bool {

		return $this->bool_enqueue_scripts;
	}

	public function getAddInlineScript() : bool {

		return $this->bool_add_inline_script;
	}

	public function getEnqueueStyles() : bool {

		return $this->bool_enqueue_styles;
	}

	public function getQSElements() : bool {

		return $this->bool_qs_elements;
	}

}
