<?php
/**
 * The interface that defines the settings / default values for the plugin.
 *
 * @package WPezGutenBetterImage\App\Config
 */

namespace WPezGutenBetterQuoteShare\App\Config;

/**
 * Defines the settings / default values methods for the plugin.
 */
interface InterfaceConfig {

	/**
	 * Construct the class.
	 *
	 * @param string $plugin_dir The plugin's dir as passed in from the bootstrap.
	 * @param string $plugin_url The plugin's url as passed in from the bootstrap.
	 */
	public function __construct( string $plugin_dir, string $plugin_url );

	/**
	 * Return the plugin's dir as passed in via the __construct();
	 *
	 * @return string
	 */
	public function getPluginDir() : string;

	/**
	 * Return the plugin's dir as passed in via the __construct();
	 *
	 * @return string
	 */
	public function getPluginURL() : string;


	/**
	 *  Return the plugin's slug, with either _ or - as a separator.
	 *
	 * @param string $separator The slug can have a separator of '_' or -.
	 *
	 * @return string
	 */
	//public function getPluginSlug( string $separator = '_' ) : string;


}
