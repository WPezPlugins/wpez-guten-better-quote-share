## WPezGutenBetter Quote Share

__Uses HTML2Canvas to convert Gutenberg Quotes and Pull Quotes into images properly sized for the various social media platforms.__

### Current status: 

- Experimental
- You WILL need CSS skills in order to customize the social-ready designs to your brand.
- FYI - HTML2Canvas can be quirky. There are some markup elements it doesn't play well with. For example, my Pull Quote block's style originally used :before and content to add the left quote mark to the background. H2C didn't like that. I had to use blackground-image and an SVG instead.  


### Overview

- Allows Admins / Editors to auto-create and save social-friendly images.
- Frontend social sharing is TODO (see below)
- See also: https://html2canvas.hertzen.com/

### TODO 

- Add interface(s) for customization filters
- Update interface for config;
- Revisit JS
- Add frontend sharing.
- On the resizing dashboard highlight active button(s)
- Clean up CSS, split out custom (for theme) from boilerplate / default.
- Fix scroll issue when adding an element to the QS overlay / canvas.


### Other

- https://www.onlinewebfonts.com/icon/314110