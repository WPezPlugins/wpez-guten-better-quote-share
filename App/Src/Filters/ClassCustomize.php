<?php

namespace WPezGutenBetterQuoteShare\App\Src\Filters;

/**
 * @implements InterfaceCoreImageCustomize
 */
class ClassCustomize implements InterfaceCustomize {

	/**
	 * The plugin's slug use to "namespace" filter and option names.
	 *
	 * @var string
	 */
	private $str_plugin_slug;

	/**
	 * The css unit values used to validated the value passed in via the method: css_unit().
	 *
	 * @var array
	 */
	private $arr_css_unit;

	/**
	 * Construct the class.
	 *
	 * @param string $str_plugin_slug The plugin's slug.
	 * @param array $arr_css_unit An array of CSS units (e.g., 'px', 'em', etc.) used for validation.
	 */
	public function __construct( string $str_plugin_slug = '', array $arr_css_unit = array() ) {

		$this->str_plugin_slug = trim( $str_plugin_slug );
		$this->arr_css_unit    = $arr_css_unit;
	}

	/**
	 * Undocumented function
	 *
	 * @param array  $arr_align_process
	 * @param string $str_block_content
	 * @param array  $arr_attrs
	 * 
	 * @return array
	 */
	public function alignProcess( array $arr_align_process, string $str_block_content, array $arr_attrs ) : array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr_align_process, $str_block_content, $arr_attrs );

		if ( is_array( $temp ) ) {
			return $temp;
		}
		return $arr_align_process;
	}



	/**
	 * Undocumented function
	 *
	 * @param float  $num
	 * @param string $context
	 *
	 * @return float
	 */
	public function contentWidth( float $num, string $context = '' ) : float {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $num, $context );
		if ( filter_var( $temp, FILTER_VALIDATE_FLOAT ) !== false ) {
			return $temp;
		}
		return $num;
	}	

	/**
	 * Undocumented function
	 *
	 * @param string $str_unit
	 * @param string $str_align
	 * @param string $str_block_content
	 * @param array $arr_attrs
	 *
	 * @return string
	 */
	public function cssUnit( string $str_unit, string $str_align, string $str_block_content, array $arr_attrs ) : string {

		$temp = trim( apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $str_unit, $str_align, $str_block_content, $arr_attrs ) );
		if ( is_string( $temp ) && ! empty( $this->arr_css_unit ) && in_array( $temp, $this->arr_css_unit, true ) ) {
			return $temp;
		}
		return $str_unit;
	}

	/**
	 * Undocumented function
	 *
	 * @param array $arr_sizes
	 * @param string $str_block_content
	 * @param array $arr_attrs
	 *
	 * @return array
	 */
	public function sizesAlignFull( array $arr_sizes, string $str_block_content, array $arr_attrs ) : array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr_sizes, $str_block_content, $arr_attrs );
		if ( is_array( $temp ) ) {
			return $temp;
		}
		return $arr_sizes;
	}

	/**
	 * Undocumented function
	 *
	 * @param array $arr_sizes
	 * @param string $str_block_content
	 * @param array $arr_attrs
	 *
	 * @return array
	 */
	public function sizesAlignWide( array $arr_sizes, string $str_block_content, array $arr_attrs ) : array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr_sizes, $str_block_content, $arr_attrs );
		if ( is_array( $temp ) ) {
			return $temp;
		}
		return $arr_sizes;
	}

	/**
	 * Undocumented function
	 *
	 * @param float $num_align_wide_ratio
	 *
	 * @return float
	 */
	public function sizesAlignWideRatio( float $num_align_wide_ratio ) : float {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $num_align_wide_ratio );
		if ( filter_var( $temp, FILTER_VALIDATE_FLOAT ) !== false ) {
			return $temp;
		}
		return $num_align_wide_ratio;
	}

	/**
	 * Undocumented function
	 *
	 * @param float $num_align_wide_fixed
	 *
	 * @return float
	 */
	public function sizesAlignWideFixed( float $num_align_wide_fixed ) : float {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $num_align_wide_fixed );
		if ( filter_var( $temp, FILTER_VALIDATE_FLOAT ) !== false ) {
			return $temp;
		}
		return $num_align_wide_fixed;
	}

	/**
	 * Undocumented function
	 *
	 * @param array $arr_sizes
	 * @param string $str_block_content
	 * @param array $arr_args
	 * @param float $num_width
	 *
	 * @return array
	 */
	public function sizesAlignCenter( array $arr_sizes, string $str_block_content, array $arr_args, float $num_width ) :array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr_sizes, $str_block_content, $arr_args, $num_width );
		if ( is_array( $temp ) ) {
			return $temp;
		}
		return $arr_sizes;
	}

	/**
	 * Undocumented function
	 *
	 * @param array $arr_sizes
	 * @param string $str_block_content
	 * @param array $arr_args
	 * @param float $num_width
	 *
	 * @return array
	 */
	public function sizesAlignLeft( array $arr_sizes, string $str_block_content, array $arr_args, float $num_width ) :array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr_sizes, $str_block_content, $arr_args, $num_width );
		if ( is_array( $temp ) ) {
			return $temp;
		}
		return $arr_sizes;
	}

	/**
	 * Undocumented function
	 *
	 * @param array $arr_sizes
	 * @param string $str_block_content
	 * @param array $arr_args
	 * @param float $num_width
	 *
	 * @return array
	 */
	public function sizesAlignRight( array $arr_sizes, string $str_block_content, array $arr_args, float $num_width ) :array {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $arr_sizes, $str_block_content, $arr_args, $num_width );
		if ( is_array( $temp ) ) {
			return $temp;
		}
		return $arr_sizes;
	}

	/**
	 * Undocumented function
	 *
	 * @param string $str_block_content
	 * @param string $str_align
	 * @param array $arr_sizes
	 * @param array $arr_sizes_custom
	 * @param array $arr_args
	 * @param float $num_width
	 *
	 * @return string
	 */
	public function modifyBlockContent( string $str_block_content, string $str_align, array $arr_sizes, array $arr_sizes_custom, array $arr_args, float $num_width ) : string {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $str_block_content, $str_align, $arr_sizes, $arr_sizes_custom, $arr_args, $num_width );
		if ( is_string( $temp ) ) {
			return $temp;
		}
		return $str_block_content;
	}

	/**
	 * Undocumented function
	 *
	 * @param float $num_max_width_ratio
	 * @param string $str_align
	 *
	 * @return float
	 */
	public function srcsetMaxWidthRatio( float $num_max_width_ratio, string $str_align ) : float {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $num_max_width_ratio, $str_align );
		if ( filter_var( $temp, FILTER_VALIDATE_FLOAT ) !== false ) {
			return $temp;
		}
		return $max_width_ratio;
	}

	/**
	 * Undocumented function
	 *
	 * @param integer $int_min_width
	 * @param string $str_align
	 * @param array $arr_wp_args
	 *
	 * @return integer
	 */
	public function srcsetMinWidth( int $int_min_width, string $str_align, array $arr_wp_args ) : int {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $int_min_width, $str_align, $arr_wp_args );
		if ( filter_var( $temp, FILTER_VALIDATE_INT ) !== false ) {
			return $temp;
		}
		return $int_max_width;
	}

	/**
	 * Undocumented function
	 *
	 * @param integer $int_max_width
	 * @param string $str_align
	 * @param array $arr_wp_args
	 *
	 * @return integer
	 */
	public function srcsetMaxWidth( int $int_max_width, string $str_align, array $arr_wp_args ) : int {

		$temp = apply_filters( $this->str_plugin_slug . '/' . __FUNCTION__, $int_max_width, $str_align, $arr_wp_args );
		if ( filter_var( $temp, FILTER_VALIDATE_INT ) !== false ) {
			return $temp;
		}
		return $int_max_width;
	}
}
