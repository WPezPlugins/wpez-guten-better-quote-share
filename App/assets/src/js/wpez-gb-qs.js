document.addEventListener("DOMContentLoaded", function() {
    
    if (typeof aisWPezGBQS !== "undefined" && aisWPezGBQS.gen.active === true ) {
        (function (ns) {

            ns.init = function(elesQs){

                const config = aisWPezGBQS;

                let elesWrap = document.querySelector(config.sel.qsEles);
                let eleOverlayOrig = elesWrap.querySelector(config.sel.overlay); //.cloneNode(true);

                let obj = {
                    config: config,
                    body: document.querySelector(config.sel.body),
                    elesWrap: elesWrap,
                    eleOverlayOrig: eleOverlayOrig,
                    styleIndex: config.gen.gStyles.start,
                    //  eleOverlay: eleOverlay,
                    //  eleOverlayToCanvas: eleOverlay.querySelector(c.selToCanvas),
                    eleShare: elesWrap.querySelector(config.sel.share),
                    elesQuotes: elesQs,
                    btnsShow: [],
                    btnsNets: [],
                    // resPxRatio: window.devicePixelRatio,
                    // resW: window.screen.width * window.devicePixelRatio,
                    // resH: window.screen.height * window.devicePixelRatio,
                }
                return obj;   
            }

            ns.addOverlay = function(obj){

                let overlayClone = obj.eleOverlayOrig.cloneNode(true);
                obj.body.appendChild(overlayClone);

                // update the obj
                obj.eleOverlay = overlayClone
                obj.eleOverlayToCanvas = overlayClone.querySelector(obj.config.sel.toCanvas);

                overlayClone.querySelector(obj.config.sel.controls).addEventListener('click', function(e){
                    
                    const eAction =  e.target.getAttribute('data-action');
                    switch(eAction){
                        case 'close':
                            ns.overlayRemove(obj, e);
                            // get the next overlay ready.
                            ns.addOverlay(obj);
                            break;

                        case 'plus':
                            ns.stylePlus(obj, e);
                            break;

                        case 'minus':
                            ns.styleMinus(obj, e);
                            break;
                        
                        case 'cite_alt':
                            ns.bcCiteAlt(obj, e);
                            break; 
                        
                        case 'cite_remove':
                            ns.bcCiteRemove(obj, e);
                            break;

                        case 'source':
                            ns.source(obj, e);
                            break; 
                        
                        case 'save':
                            ns.makeImage(obj, e);
                            break;
                    }
                });

            }

            ns.source = function(obj, e){

                let eleSource = obj.eleOverlayToCanvas.querySelector(obj.config.sel.source);

                if (eleSource === null ){
                    alert('Sorry, adding the Source is not possible at this time.');
                } else {

                    if (0 < eleSource.innerText.length){
                        eleSource.innerText = '';
                    } else {
                        eleSource.innerText = obj.config.gen.source_prefix + obj.config.gen.source;
                    }


                }
            }

            ns.bcCiteRemove = function(obj, e){
                
                obj.eleOverlay.classList.toggle(obj.config.sel.classCiteRemove);
            }

            ns.bcCiteAlt = function(obj, e){

                if ( ! obj.eleOverlay.classList.contains('wp-block-quote') ){

                    // TODO - add CSS to hide this button if it's not a block quote
                    alert('Sorry, this isn\'t a Block Quote.' )
                } else {

                     if (obj.config.gen.blockquoteCiteAlt === false){

                        alert('Sorry, there is no Alt Cite value in the config. Please contact the site Admin.' )
                     
                    } else {
                        
                        let eleBQ = obj.eleOverlayToCanvas.querySelector('blockquote.wp-block-quote');
                        let eleCite = eleBQ.querySelector('cite');
                        
                        if (eleCite === null) {
                            
                            eleCite = document.createElement('cite');
                            eleBQ.appendChild(eleCite);
                             
                        }
                        eleCite.innerText = obj.config.gen.blockquoteCiteAlt;
                        obj.eleOverlay.classList.add(obj.config.sel.classCiteAlt);
                    }
                }
            }


            

            ns.stylePlus = function(obj, e){

                if ( (obj.styleIndex+1) <= obj.config.gen.gStyles.end ){
                    
                    obj.eleOverlayToCanvas.classList.remove(obj.config.gen.gStyles.slug + obj.styleIndex);
                    obj.styleIndex++;
                    obj.eleOverlayToCanvas.classList.add(obj.config.gen.gStyles.slug + obj.styleIndex);
                }

            }

             ns.styleMinus = function(obj, e){

                if ( obj.config.gen.gStyles.start <= (obj.styleIndex-1) ){
                    
                    obj.eleOverlayToCanvas.classList.remove(obj.config.gen.gStyles.slug + obj.styleIndex);
                    obj.styleIndex--;
                    obj.eleOverlayToCanvas.classList.add(obj.config.gen.gStyles.slug + obj.styleIndex);
                }

            }

            ns.overlayRemove = function(obj, e){
                //reset, remove
                obj.body.classList.remove(obj.config.sel.classOverlayOpen);
                obj.eleOverlay.remove();
            }

            ns.makeImage = function(obj, e){
                let active = obj.eleOverlay.getAttribute('data-active');
                let id = obj.eleOverlay.getAttribute('data-id');
                if (id === null){
                    id = '_';
                } else {
                    id = '_' + id + '_';
                }
                
                h2cArgs = {};
                if (obj.config.gen.h2cScale !== 'default'){
                    h2cArgs.scale = obj.config.gen.h2cScale;
                }
                html2canvas(obj.eleOverlayToCanvas, h2cArgs).then(function (canvas) {
                    ns.download(canvas.toDataURL(), obj.config.gen.fileNameSlug + id + active + '.png');
                });
            }

            ns.download = function(dataurl, filename) {
                const link = document.createElement("a");
                link.href = dataurl;
                link.download = filename;
                link.click();
                // TODO - close the overlay
            }

            ns.addShareComponents = function(obj){

                let eleShareClone;
                obj.elesQuotes.forEach(function(ele, ndx) {
                    if ( ! ele.classList.contains(obj.config.sel.quotesEx)){
                        eleShareClone =  obj.eleShare.cloneNode(true);
                        ele.insertAdjacentElement('beforeend', eleShareClone);

                        obj.btnsShow[ndx] = eleShareClone.querySelector(obj.config.sel.btnShow);
                        ns.btnsShowListener(obj, ndx);

                        obj.btnsNets[ndx] = eleShareClone.querySelector(obj.config.sel.networkWrap).querySelectorAll(obj.config.sel.networkEle);
                        ns.btnsNetworksListener(obj, ndx);
                    }
                });
            }

            ns.btnsShowListener = function(obj, ndx){
                
                obj.btnsShow[ndx].addEventListener("click", function (e) {

                    if (e.target.parentNode.classList.contains(obj.config.sel.classbBtnShowHide) === true){
                        e.target.parentNode.classList.remove(obj.config.sel.classbBtnShowHide);
                    } else {
                        e.target.parentNode.classList.add(obj.config.sel.classbBtnShowHide);
                    }
                });
            }

            ns.btnsNetworksListener = function(obj, ndx){

                obj.btnsNets[ndx].forEach(function(ele, btndx){
                    obj.btnsNets[ndx][btndx].addEventListener('click', function(e){
                        let dataSize = e.target.getAttribute('data-size');                    
                        ns.prepToCanvas(obj, ndx, dataSize);

                    });
                })
            }

            // TODO - refactor so it better "integrates" with the other functions.
            ns.overlayNetworks = function(obj, ndx, dataSize, eleQuoteClone){

                let eleNetsWrap = eleQuoteClone.querySelector(obj.config.sel.shareNetworks);
                obj.eleOverlay.prepend(eleNetsWrap);
                eleNetsWrap.addEventListener('click', function(e){
                    
                    let dataSize = e.target.getAttribute('data-size');
                    if (dataSize !== null){
                        obj.eleOverlay.setAttribute('data-active', dataSize);
                        ns.styleLoop(obj, ndx, dataSize, eleQuoteClone);
                    }

                });


            }

            ns.prepToCanvas = function(obj, ndx, dataSize){

                let eleQuoteClone = obj.elesQuotes[ndx].cloneNode(true);

                ns.overlayNetworks(obj, ndx, dataSize, eleQuoteClone);

                // TODO can the open be applied to the overlay?
                obj.body.classList.add(obj.config.sel.classOverlayOpen);
                obj.eleOverlay.setAttribute('data-active', dataSize);
                if (0 < eleQuoteClone.id.length){
                    obj.eleOverlay.setAttribute('data-id', eleQuoteClone.id);
                }

                // If the Quote ele (wrapper) has a class that starts with 'wp-block-' || 'wpez-qs-' then add those classes to our cloned ele.
                obj.elesQuotes[ndx].classList.forEach(function(v, x){ 
                    if ( v.startsWith('wp-block-') || v.startsWith('wpez-qs-')){
                        obj.eleOverlay.classList.add(v);
                    }
                });

                obj.eleOverlayToCanvas.appendChild(eleQuoteClone);
                ns.styleLoop(obj, ndx, dataSize, eleQuoteClone);
            }

            ns.styleLoop = function(obj, ndx, dataSize, eleQuoteClone){

                if ( obj.config.gen.gStyles.active !== true ){
                    return;
                }
                // TODO - this "reset" bit feels janky. Refactor?
                obj.eleOverlayToCanvas.classList = '';
                obj.eleOverlayToCanvas.classList.add('wpez-qs-to-canvas');
                let lastStyle = obj.config.gen.gStyles.slug + obj.config.gen.gStyles.start;
                obj.eleOverlayToCanvas.classList.add(lastStyle);
                for (let reSize = obj.config.gen.gStyles.start; reSize <= obj.config.gen.gStyles.end; reSize++){
                    obj.eleOverlayToCanvas.classList.add(obj.config.gen.gStyles.slug + reSize);
                    obj.eleOverlayToCanvas.classList.remove(lastStyle);
                    if ( eleQuoteClone.offsetHeight < obj.config.net[dataSize].maxH ){
                        // there's still room. keep trying.
                        lastStyle = obj.config.gen.gStyles.slug + reSize;
                        obj.styleIndex = reSize
                    } else {
                        // We went too big. Backup one and "break" out.
                         obj.eleOverlayToCanvas.classList.add(lastStyle);
                         obj.eleOverlayToCanvas.classList.remove(obj.config.gen.gStyles.slug + reSize);
                         obj.styleIndex = reSize - 1
                        break;
                    }
                }
            }

        })(aisWPezGBQS.gen.ns);

        let elesQuotes = document.querySelectorAll(aisWPezGBQS.sel.quotes);
        if (0 < elesQuotes.length){
            
           let objInit = aisWPezGBQS.gen.ns.init(elesQuotes);
           aisWPezGBQS.gen.ns.addOverlay(objInit);
           aisWPezGBQS.gen.ns.addShareComponents(objInit);
        }

    } else {
        console.warn( 'aisWPezGBSS not found')
    }

});
