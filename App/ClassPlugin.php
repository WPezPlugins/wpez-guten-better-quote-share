<?php
/**
 * Class that coordinates the plugin's classes and hooks.
 *
 * @package WPezGutenBetterImage\App
 */

namespace WPezGutenBetterQuoteShare\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}


use WPezGutenBetterQuoteShare\App\{
	Lib\HooksRegister\ClassHooksRegister as HooksRegister,
	Config\InterfaceConfig,
	Src\Filters\ClassCustomize as Customize,
	Src\ClassBetterQuoteShare as BetterQuoteShare,
};


/**
 * Coordinates the plugin's classes and hooks.
 */
class ClassPlugin {

	/**
	 * Instance of the ClassHooksRegister class.
	 *
	 * @var object
	 */
	protected $new_hooks_reg;

	/**
	 * Array of the plugin's actions.
	 *
	 * @var array
	 */
	protected $arr_actions;

	/**
	 * Array of the plugin's filters.
	 *
	 * @var array
	 */
	protected $arr_filters;

	/**
	 * Instance of the CoreImage class.
	 *
	 * @var object
	 */
	protected $new_better_quote_share;

	/**
	 * Let's construct.
	 *
	 * @param Config $new_config Instance of the ClassConfig class.
	 */
	public function __construct( InterfaceConfig $new_config ) {

			$this->setPropertyDefaults( $new_config );

			$this->actions( true );

			$this->filters( false );

			// this must be last.
			$this->hooksRegister();
	}

	protected function setPropertyDefaults( $new_config ) {

		$this->new_hooks_reg = new HooksRegister();
		$this->arr_actions   = array();
		$this->arr_filters   = array();

		// $new_better_quote_share_customize = new Customize( $new_config->getPluginSlug(), $new_config->getCssUnitAcceptedValues() );
		$new_better_quote_share_customize = new Customize( 'getPluginSlug', array());
        $this->new_better_quote_share = new BetterQuoteShare( $new_config, $new_better_quote_share_customize );
	}


	/**
	 * After gathering (below) the arr_actions and arr_filter, it's time to
	 * make some RegisterHook magic
	 */
	protected function hooksRegister() {

		$this->new_hooks_reg->loadActions( $this->arr_actions );

		$this->new_hooks_reg->loadFilters( $this->arr_filters );

		$this->new_hooks_reg->doRegister();
	}

	/**
	 * Setup the plugin's actions. There are currently no actions, only filters.
	 *
	 * @param boolean $bool A flag for toggling the method.
	 *
	 * @return void
	 */
	public function actions( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_loaded',
			'component' => $this->new_better_quote_share,
			'callback'  => 'setCurrentUserCan',
			'priority'  => 10,
        );

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_better_quote_share,
			'callback'  => 'enqueueScripts',
			'priority'  => 10,
        );

        $this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_better_quote_share,
			'callback'  => 'addInlineScript',
			'priority'  => 20,
        );

        $this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_enqueue_scripts',
			'component' => $this->new_better_quote_share,
			'callback'  => 'enqueueStyles',
			'priority'  => 10,
        );

		$this->arr_actions[] = array(
			'active'    => true,
			'hook'      => 'wp_footer',
			'component' => $this->new_better_quote_share,
			'callback'  => 'qsElements',
			'priority'  => 10,
        );

        
		
	}

	/**
	 * Setup the plugin's filters.
	 *
	 * @param boolean $bool A flag for toggling the method.
	 *
	 * @return void
	 */
	public function filters( $bool = true ) {

		if ( true !== $bool ) {
			return;
		}

		// Ref ('Expand full source code' to see filter): https://developer.wordpress.org/reference/functions/wp_calculate_image_sizes/ .
		//
		// as of v 0.0.9
		// active = false - apparently this filter only fires once for all images, as opposed to once per image. 
		// as a result, we don't need this any more. but the code stays until this is 100% confirmed.
		$this->arr_filters[] = array(
			'active'        => false,
			'hook'          => 'wp_calculate_image_sizes',
			'component'     => $this->new_better_quote_share,
			'callback'      => 'wpCalculateImageSizes',
			'priority'      => 20,
			'accepted_args' => 5,
		);

	}
}
