<?php
/**
 * The core functionality of the plugin.
 * 
 * @package WPezGutenBetterImage\App\Src
 */

namespace WPezGutenBetterQuoteShare\App\Src;

use WPezGutenBetterQuoteShare\App\Config\InterfaceConfig as Config;
use WPezGutenBetterQuoteShare\App\Src\Filters\InterfaceCustomize as Customize;



// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * The core functionality - methods for the WP filters in ClassPlugin - of the plugin.
 */
class ClassBetterQuoteShare {

	/**
	 * Instance of the config class.
	 *
	 * @var object
	 */
	private $new_config;

	/**
	 * Instance of the customize class.
	 *
	 * @var object
	 */
	private $new_customize;

	/**
	 * The plugin's slug (from the config).
	 *
	 * @var string
	 */
	private $str_plugin_slug;


	private $bool_current_user_can;



	/**
	 * Construct the class.
	 *
	 * @param Config             $new_config Instance of the config class.
	 * @param Customize $new_core_image_customize Instance of the customize class.
	 */
	public function __construct( Config $new_config, Customize $new_customize ) {

		$this->new_config    = $new_config;
		$this->new_customize = $new_customize;
		$this->setPropertyDefaults();
	}


	/**
	 * Sets the defaults for the properties.
	 *
	 * @return void
	 */
	private function setPropertyDefaults() {

		$this->bool_current_user_can = false;
	}


	public function setCurrentUserCan() {

		if ( current_user_can( $this->new_config->getCurrentUserCanCapability() ) ) {

			$this->bool_current_user_can = true;
		}
	}


	public function enqueueScripts() {

		if ( true === $this->bool_current_user_can && true === $this->new_config->getEnqueueScripts() ) {
		
			// https://html2canvas.hertzen.com/?ref=hackernoon.com
			\wp_enqueue_script(
				'html2canvas',
				$this->new_config->getPluginURL() . '/App/assets/dist/js/html2canvas.min.js',
				array(),
				'1.3.2',
				false,
			);

			\wp_enqueue_script(
				'wpez_gb_qs_js',
				$this->new_config->getPluginURL() . '/App/assets/src/js/wpez-gb-qs.js',
				array( 'html2canvas' ),
				'0.0.0',
				true,
			);
		}
	}

	public function addInlineScript() {

		if ( true === $this->bool_current_user_can && true === $this->new_config->getAddInlineScript() ) {

			global $post;

			$arr_gen                      = $this->new_config->getJSGeneral();
			$arr_gen['fileNameSlug']      = $post->post_name;
			$author_meta_key              = $this->new_config->getCiteAltAuthorMetaKey();
			$arr_gen['blockquoteCiteAlt'] = get_the_author_meta( $author_meta_key, $post->post_author );
			$arr_gen['source']            = $post->post_title;

			$arr_args = array(

				'gen' => $arr_gen,
				'net' => $this->new_config->getJSNetworks(),
				'sel' => $this->new_config->getJSSelectors(),

			);

			// TODO get const name from Config.
			wp_add_inline_script(
				'wpez_gb_qs_js',
				'const aisWPezGBQS = ' . json_encode( $arr_args ),
				'before',
			);
		}
	}


	public function enqueueStyles() {

		if ( true === $this->bool_current_user_can && true === $this->new_config->getEnqueueStyles() ) {

			wp_enqueue_style(
				'wpez_gb_qs_css',
				$this->new_config->getPluginURL() . '/App/assets/src/css/wpez-gb-qs.css',
				array(),
				'0.0.0',
				'all',
			);
		}
	}

	// TODO - Base more of this off the config. But for now, it'll do. 
	public function qsElements() {

		if ( true === $this->bool_current_user_can && true === $this->new_config->getQSElements() ) {

			?>
<div class="wpez-qs-elements"> 
	<div class="wpez-qs-overlay">
		<div class = "wpez-qs-controls">
			<button data-action="minus">Style: Down -</button>
			<button data-action="plus">Style: Up +</button>
			<button data-action="cite_alt">Cite: Alt (BQ only)</button>
			<button data-action="cite_remove">Cite: Remove</button>
			<button data-action="source">Source</button>
			<button data-action="save">Save</button>
			<button data-action="close">Close X</button>
		</div>
	<div class="wpez-qs-to-canvas">
		<div class="wpez-qs-source"></div>
		<div class="wpez-qs-site-wrap">
			<div class="wpez-qs-site-name">Alchemy United</div>
			<div class="wpez-qs-site-tagline">Understanding Is Power</div>
		</div>  
	</div>
</div>
	<div class="wpez-qs-share-wrap wpez-qs-networks-display-false">
		<button class="wpez-qs-networks-show">share</button>
		<div class="wpez-qs-networks-wrap">
			<button data-size="feed_fb">FB</button>
			<button data-size="feed_ig">IG</button>
			<button data-size="feed_li">LI</button>
			<button data-size="feed_pin">PIN</button>
			<button data-size="feed_tw">Tw</button>
			<button data-size="story">Other: Story</button>
		</div>
	</div>
</div>
			<?php
		}
	}
}
